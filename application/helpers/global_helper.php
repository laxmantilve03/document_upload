<?php
function send_sms($mobile,$message)
{
    if (isset($mobile) && $mobile != '') {
        //get the inputs
        $mobile = $mobile;
        $message = $message;
        $fields = array(
            "route" => "v3",
            "message" => $message,
            "sender_id" => "FTWSMS", 
            "language" => "english",
            "flash" => 0,
            "numbers" => $mobile,
        );

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://www.fast2sms.com/dev/bulkV2",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($fields),
            CURLOPT_HTTPHEADER => array(
                "authorization: 3qCzWvLZSnPbO9iwAx8EgH2ol0rTyap5YdDQXG7ecRhN6mVKsuEeNItJOoUufDpaq32y8bnl0Ymi7jR6",
                "accept: */*",
                "cache-control: no-cache",
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return false;
        } else {
     
            return true;
        }
    }
}
