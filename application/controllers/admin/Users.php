<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users extends CI_Controller
{

	function __construct()
	{
		parent::__construct();

		$this->load->model('admin/Users_model', 'users_model');
	}
	function index()
	{
		$this->load->view('admin/users');
	}

	function add()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('mobile', 'Mobile', 'required|min_length[10]|max_length[10]');
		if ($this->form_validation->run()) {

			$params['mobile'] = $this->input->post('mobile');
			$params['created_by'] = $this->encryption->decrypt($_SESSION['admin_user']);
			$this->users_model->add_user($params);

			$this->session->set_flashdata('message', 'User Added Successfully.');
		} else {
			$this->session->set_flashdata('message', 'Invalid Mobile Number.');
		}
		$this->load->view('admin/users');
	}
}
