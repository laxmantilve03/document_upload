<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Authentication extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Authentication_model', 'authentication_model');
	}

	public function login()
	{
		$this->load->view('login');
	}

	function verify_login()
	{

		$mobile = $_POST['mobile'];
		//Check If Mobile Number Exsist in Database and Is Active
		$validate_admin = $this->authentication_model->verify_user($mobile);

		if ($validate_admin['user_count'] == 1) {
			//Admin Present 
			//Send OTP

			$otp = rand(100000, 999999);
			$message = 'QB OTP : ' . $otp;
			if(send_sms($otp,$message)){
				$log_params['mobile'] = $mobile;
				$log_params['otp'] = $otp;
				$log_params['action'] = 'login-otp';
				$log_params['status'] = 'success';
				$log_params['created_by'] = $this->input->ip_address();
	
				$this->authentication_model->add_user_log($log_params);
	
				$data['mobile'] = $mobile;
				$this->load->view('otp.php', $data);
			}else{
				$this->session->set_flashdata('error', 'Unable to Send OTP.Please Try After sometime or Contact Support.');
			redirect('/');
			}

			
		} else {
			$this->session->set_flashdata('error', 'Unauthorized Login!! Enter Correct Mobile Number and Try again.');
			redirect('/');
		}
	}

	function verify_otp()
	{

		$mobile = $this->input->post('mobile');
		$otp = $this->input->post('otp');

		$otp_data = $this->authentication_model->check_otp($mobile);

		if (isset($otp_data) && $otp_data['otp'] == $otp) {
			$user_data = $this->authentication_model->get_user($mobile);
			$this->session->set_userdata('user', $this->encryption->encrypt($user_data['id']));
			echo 200;
		} else {
			echo 204;
		}
	}

	function logout()
	{
		$this->session->unset_userdata('user');
		redirect('/');
	}
}
