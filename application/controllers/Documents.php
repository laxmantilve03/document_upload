<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Documents extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Documents_model', 'documents_model');
	}

	function index()
	{
		//Get Documents List
		$user_session = $this->session->userdata('user');
		$user = $this->encryption->decrypt($user_session);
		$documents = $this->documents_model->get_documents($user);

		$data['documents'] = $documents;
		$this->load->view('documents', $data);
	}

	function do_upload()
	{
		$config['upload_path']          = './uploads/';
		$config['allowed_types']        = 'jpg|png';
		$config['max_size']             = 6000;


		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('docs')) {
			$this->session->set_flashdata('message', $this->upload->display_errors());
		} else {
			$data = $this->upload->data();
			$user_session = $this->session->userdata('user');
			$user = $this->encryption->decrypt($user_session);

			$params['file_name'] = $data['file_name'];
			$params['document_path'] = $data['file_path'];
			$params['user_id'] = $user;
			$params['created_by'] = $this->input->ip_address();
			$documents = $this->documents_model->add_documents($params);
			$this->session->set_flashdata('message', 'Document Uploaded Successfully.');
		}
		redirect('documents');
	}
}
