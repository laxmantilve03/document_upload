<?php

class Documents_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    function get_documents($id)
    {
        $this->db->select('*');
        $this->db->from('mst_documents');
        $this->db->where('user_id', $id);
        $this->db->order_by("id", "desc");
        return $this->db->get()->result_array();
    }

    function add_documents($params)
    {
        $this->db->insert('mst_documents', $params);
        return $this->db->insert_id();
    }
}
