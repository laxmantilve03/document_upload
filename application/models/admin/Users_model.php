<?php

class Users_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    function get_user($mobile)
    {
        $this->db->select('*');
        $this->db->from('mst_users');
        $this->db->where('mobile', $mobile);
        $this->db->where('is_active', '1');
        return $this->db->get()->row_array();
    }

    function add_user($params)
    {
        $this->db->insert('mst_users', $params);
        return $this->db->insert_id();
    }

 
}
