<?php

class Authentication_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    function get_admin($mobile)
    {
        $this->db->select('*');
        $this->db->from('mst_admin');
        $this->db->where('mobile', $mobile);
        $this->db->where('is_active', '1');
        return $this->db->get()->row_array();
    }

    function verify_admin($mobile)
    {
        $this->db->select('count(id) as admin_count');
        $this->db->from('mst_admin');
        $this->db->where('mobile', $mobile);
        $this->db->where('is_active', '1');
        return $this->db->get()->row_array();
    }

    function add_admin_log($params)
    {
        $this->db->insert('mst_admin_log', $params);
        return $this->db->insert_id();
    }

    function check_otp($mobile)
    {
        $this->db->select('otp');
        $this->db->from('mst_admin_log');
        $this->db->where('mobile', $mobile);
        $this->db->order_by("id", "desc");
        $this->db->limit(1);
        return $this->db->get()->row_array();
    }
}
