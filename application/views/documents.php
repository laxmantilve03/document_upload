<a href='logout'>Logout</a>
<br>

User Login - Documents
<?php
 echo form_open_multipart('document-save');
?>
<input type="file" name='docs' />
<input type='submit' value='Upload'>

<?php echo form_close();

echo $this->session->flashdata('error');

if (isset($documents) && count($documents) > 0) {
?>
    <table>
        <tr>
            <th>Sr No</th>
            <th>Document Name</th>
            <th>Date</th>
        </tr>
        <?php
        $i = 1;
        foreach ($documents as $key => $value) { 
             ?><tr>
            <td><?= $i ?></td>
            <td><?= $value['file_name'] ?></td>
            <td><?= date("d-m-Y h:i:s", strtotime($value['created_on'])); ?></td></tr>
        <?php $i++;   }
        ?>
    </table>

<?php
}
