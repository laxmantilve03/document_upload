User OTP Verification

<input type="hidden" name='mobile' id='mobile' value='<?= $mobile ?>'>
<input type="text" name='otp' value='' id='otp'>
<button type='button' value='Submit' onclick="verify_otp();"> Submit </button>


<div class="error"></div>
<script src="https://code.jquery.com/jquery-3.5.0.js"></script>
<script>
    function verify_otp() {
        var mobile = $('#mobile').val();
        var otp = $('#otp').val();

        $.ajax({
            url: 'user-verify-otp',
            type: 'POST',
            data: {
                mobile: mobile,
                otp: otp
            },
            success: function(response) {
                if (response == 200) {
                    window.location.href = 'documents';
                } else {
                    $('.error').html('Wrong OTP!! Please Enter Correct OTP.');
                }

            },
            error: function(errorThrown) {
                $('.error').html(errorThrown);
            }
        });
    }
</script>