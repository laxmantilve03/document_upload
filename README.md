# document_upload

Project Consist of Admin and User Logins with OTP Only. This is Simple Document Upload Module.

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Extract the files in server Folder.
Set base_url in application/config/config.php as your application url
Create database and import file qb.sql
Set Database connnection details in  application/config/database.php

Goto Database->mst_admin table and add one admin entry with valid 10 digits mobile number so you can login into Admin.
Goto myapplicationurl/admin and enter your mobile number and otp to login.
Then Goto Users Link and add users.

Users can Login using OTP from application URL.

User can upload document after login.

## Roadmap
Following Features Can be Added in further development.

Reponsive UI using Bootstrap.
CSRF Token Enabled for Security.
Authentication Checks on all Links.
Document and Users List will also visible to Admin.



